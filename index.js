const Express = require('express');
const bodyParser = require('body-parser');
const router = require('./src/routers/main');
const app = Express();
global.cfg = require('./config.js');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(router);

app.listen(5645, 'localhost', () => {
    console.log(`server is listening on ${cfg.protocol}://${cfg.host}:${cfg.port}`);
});
