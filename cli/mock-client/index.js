const axios = require('axios');
const cfg = require('../../config');
const moment = require('moment');

(async () => {
    let count = 1;
    let errorsCount = 0;
    let successCount = 0;
    let maxTime = 0;
    let minTime = 999999;
    let avgTime = 0;

    let hash = {};
    for(let i = 1; i < cfg.max_mock_clients; i++) {
        hash[i] = true;
    }

    let clientGenerator = setInterval(() => {
        let client_id = `${count}`;
        if(count > cfg.max_mock_clients - 1) clearInterval(clientGenerator);
        count++;
        console.log(`Client #${client_id} started at: ` + moment().format('YYYY-MM-DD HH:mm:ss'));
        const requestStart = Date.now();
        axios.get(cfg.proxy_rest_path())
            .then(res => {
                console.log(`Client #${client_id} success. Found lines: ` + res.data.data.length);
                successCount++;
            })
            .catch(err => {
                console.error(`Client #${client_id} failed. Error: ` + err.message);
                errorsCount++;
            })
            .finally(() => {
                delete hash[client_id];
                let time = (Date.now() - requestStart) / 1000;
                if(time > maxTime) maxTime = time;
                else if(time < minTime) minTime = time;
                avgTime+=time;
                console.log(`Client #${client_id} - Request finished in ${time} seconds`);
                // console.log(Object.keys(hash).length);
                // console.log(hash);
                if(Object.keys(hash).length === 0) {
                    console.log('Errors count: ' + errorsCount);
                    console.log('Success count: ' + successCount);
                    console.log('Avg time: ' + maxTime / cfg.max_mock_clients);
                    console.log('Max time: ' + maxTime);
                    console.log('Min time: ' + minTime);
                }
            })

    }, 300)
})();
