const express = require('express');
const router = express.Router();
const axios = require('axios');

router.get('/', async (req, res) => {
    // res.json({ status: 'ok', data: { query: req.query, body: req.body } } )
    try {
        // @todo - add handling of co-current requests to some mock service
        let parr = [];
        for(let i = 0; i < 20; i++) {
            parr.push(axios.get(cfg.mock_rest_path() + '/get-mock-data/' + i))
        }
        let result = await Promise.all([...parr]);
        let data = result.map((e, idx) => result[idx].data.data)
        res.json({success: true, data});
    } catch (e) {
        console.error(e);
        res.json({success: false, msg: 'Service crashed. Error: ' + e.message});
    }
});

module.exports = router;
