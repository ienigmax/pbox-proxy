const config =  {
    port: 5645,
    host: "localhost",
    protocol: "http",
    max_mock_clients: 50,
    min_entries: 100,
    max_entries: 200
}

const paths = {
    proxy_rest_path: () => `${config.protocol}://${config.host}:${config.port}/proxy`,
    mock_rest_path: () => `${config.protocol}://${config.host}:${config.port}/mock-rest`,
}
module.exports = {...config, ...paths}
