const express = require('express'),
    router = express.Router(),
    proxy = require('../contellers/proxy'),
    mockRest = require('../contellers/mock-rest')
router.use('/proxy', proxy)
router.use('/mock-rest', mockRest)
module.exports = router;
