const express = require('express');
const router = express.Router();
const Fakerator = require("fakerator");
const fakerator = Fakerator("de-DE");

const randRange = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;
const sleep = (sec) => new Promise(((resolve, reject) => setTimeout(() => resolve, sec * 1000)))

router.get('/get-mock-data/:id', async (req, res) => {
    try {
        let arr = [];
        for(let i = 0; i < randRange(cfg.min_entries, cfg.max_entries); i++) {
            arr.push({
                first_name: fakerator.names.firstName(),
                last_name: fakerator.names.firstNameM(),
                email: fakerator.internet.email(),
                ip: fakerator.internet.ip(),
                ipv6: fakerator.internet.ipv6(),
                note1: fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph(),
                note2: fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph(),
                note3: fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph(),
                note4: fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph(),
                note5: fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph(),
                note6: fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph(),
                note7: fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph(),
                note8: fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph(),
                note9: fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph(),
                note10: fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph() + ' ' + fakerator.lorem.paragraph(),
            });
        }
        // await sleep(1 + randRange(1,3));
        return res.json({success: true, data: { request_id: req.params.id, data: arr }});
    } catch (e) {
        console.error(e);
        res.json({success: false, msg: 'Mock Rest crashed. Error: ' + e.message});
    }
})

module.exports = router;
